package ru.peo.playandlearn;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


@SuppressLint("Registered")
public class MainActivity extends AppCompatActivity {
    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, LessonsActivity.class);
        startActivity(intent);
    }
}
