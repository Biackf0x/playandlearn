package ru.peo.playandlearn;

import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SynthesizerActivity extends AppCompatActivity implements View.OnClickListener{

        Button B_C,B_D,B_E,B_F,B_G,B_A,B_B,
                b_c, b_d, b_f, b_g,b_a;

        private SoundPool soundPool;
        private int sound_C, sound_D, sound_E, sound_F, sound_G, sound_A, sound_B,
                sound_c, sound_d, sound_f, sound_g, sound_a;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //для альбомного режима

            B_C = findViewById(R.id.BtnC);
            B_D = findViewById(R.id.BtnD);
            B_E = findViewById(R.id.BtnE);
            B_F = findViewById(R.id.BtnF);
            B_G = findViewById(R.id.BtnG);
            B_A = findViewById(R.id.BtnA);
            B_B = findViewById(R.id.BtnB);
            b_c = findViewById(R.id.Btnc);
            b_d = findViewById(R.id.Btnd);
            b_f = findViewById(R.id.Btnf);
            b_g = findViewById(R.id.Btng);
            b_a = findViewById(R.id.Btna);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                soundPool = new SoundPool.Builder().setMaxStreams(5).build();
            } else {
                soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
            }

            sound_C = soundPool.load(this, R.raw.c, 1);
            sound_D = soundPool.load(this, R.raw.d, 1);
            sound_E = soundPool.load(this, R.raw.e, 1);
            sound_F = soundPool.load(this, R.raw.f, 1);
            sound_G = soundPool.load(this, R.raw.g, 1);
            sound_A = soundPool.load(this, R.raw.a, 1);
            sound_B = soundPool.load(this, R.raw.b, 1);
            sound_c = soundPool.load(this, R.raw.ccc, 1);
            sound_d = soundPool.load(this, R.raw.dd, 1);
            sound_f = soundPool.load(this, R.raw.ff, 1);
            sound_g = soundPool.load(this, R.raw.gg, 1);
            sound_a = soundPool.load(this, R.raw.aa, 1);

            // устанавливаем один обработчик для всех кнопок
            B_C.setOnClickListener(this);
            B_D.setOnClickListener(this);
            B_E.setOnClickListener(this);
            B_F.setOnClickListener(this);
            B_G.setOnClickListener(this);
            B_A.setOnClickListener(this);
            B_B.setOnClickListener(this);
            b_c.setOnClickListener(this);
            b_d.setOnClickListener(this);
            b_f.setOnClickListener(this);
            b_g.setOnClickListener(this);
            b_a.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.BtnC:
                    soundPool.play(sound_C, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnD:
                    soundPool.play(sound_D, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnE:
                    soundPool.play(sound_E, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnF:
                    soundPool.play(sound_F, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnG:
                    soundPool.play(sound_G, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnA:
                    soundPool.play(sound_A, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnB:
                    soundPool.play(sound_B, 1, 1, 0, 0, 1);
                    break;
                case R.id.Btnc:
                    soundPool.play(sound_c, 1, 1, 0, 0, 1);
                    break;
                case R.id.Btnd:
                    soundPool.play(sound_d, 1, 1, 0, 0, 1);
                    break;
                case R.id.Btnf:
                    soundPool.play(sound_f, 1, 1, 0, 0, 1);
                    break;
                case R.id.Btng:
                    soundPool.play(sound_g, 1, 1, 0, 0, 1);
                    break;
                case R.id.Btna:
                    soundPool.play(sound_a, 1, 1, 0, 0, 1);
                    break;
            }
        }
    }