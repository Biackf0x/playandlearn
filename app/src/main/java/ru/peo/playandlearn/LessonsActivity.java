package ru.peo.playandlearn;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LessonsActivity extends AppCompatActivity {

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);
    }
}
